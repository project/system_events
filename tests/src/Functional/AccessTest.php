<?php declare(strict_types = 1);

namespace Drupal\Tests\system_events\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests access for system events routes.
 *
 * @group system_events
 */
final class AccessTest extends BrowserTestBase {

  protected $defaultTheme = 'stable';

  protected static $modules = ['system_events'];

  public function testAccess(): void {
    $this->drupalGet('/admin/reports/system-event');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/admin/structure/system_event_types');
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser([]);
    $this->drupalLogin($user);
    $this->drupalGet('/admin/reports/system-event');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/admin/structure/system_event_types');
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser(['administer system event types']);
    $this->drupalLogin($user);
    $this->drupalGet('/admin/reports/system-event');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('/admin/structure/system_event_types');
    $this->assertSession()->statusCodeEquals(200);
  }

}
