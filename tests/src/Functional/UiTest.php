<?php

namespace Drupal\Tests\system_events\Functional;

use Drupal\system_events\Entity\SystemEvent;
use Drupal\system_events\Entity\SystemEventType;
use Drupal\Tests\BrowserTestBase;

/**
 * A test for system events pages.
 *
 * @group system_events
 */
final class UiTest extends BrowserTestBase {

  protected $defaultTheme = 'stable';

  protected static $modules = ['system_events'];

  protected function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser(['administer system event types']);
    $this->drupalLogin($user);
  }

  public function testReportPage(): void {
    $this->drupalGet('/admin/reports/system-event');
    $this->assertSession()->titleEquals('System Events | Drupal');
    $this->assertSession()->pageTextContains('There are no system events yet.');

    self::createEvents();
    $this->drupalGet('/admin/reports/system-event');

    $cells = $this->xpath('//table/tbody/tr[1]/td');
    self::assertRow($cells, '1', 'Alpha', '', 'No');

    $cells = $this->xpath('//table/tbody/tr[2]/td');
    self::assertRow($cells, '3', 'Alpha', '', 'No');

    $cells = $this->xpath('//table/tbody/tr[3]/td');
    self::assertRow($cells, '2', 'Alpha', '60', 'Yes');
  }

  public function testCanonicalPage(): void {
    self::createEvents();
    $this->drupalGet('/admin/reports/system-event');

    // Make sure the page is rendered in admin theme.
    // @see \Drupal\system_events\Routing\SystemEventRouteProvider
    $this->assertSession()->responseMatches('/"currentPathIsAdmin":true/');

    $link = $this->xpath('//table/tbody/tr[3]/td[1]/a')[0];
    $link->click();

    $this->assertSession()->pageTextContains('Event #2');
    $type = $this->xpath('//div[text() = "Type"]/following-sibling::div[text() = "Alpha"]');
    self::assertCount(1, $type);
    $created = $this->xpath('//div[text() = "Created"]');
    self::assertCount(1, $created);
    $max_age = $this->xpath('//div[text() = "Max Age"]/following-sibling::div[text() = "1 min"]');
    self::assertCount(1, $max_age);
    $expired = $this->xpath('//div[text() = "Expired"]/following-sibling::div[text() = "Yes"]');
    self::assertCount(1, $expired);
  }

  public function testEventTypes(): void {
    $this->drupalGet('/admin/structure/system_event_types');
    $this->assertSession()->titleEquals('System Event types | Drupal');
    $this->assertSession()->pageTextContains('No system event types available.');

    $this->getSession()->getPage()->clickLink('Add system event type');
    $this->assertSession()->titleEquals('Add system event type | Drupal');
    $edit = [
      'Label' => 'Alpha',
      'Machine-readable name' => 'alpha',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('The system event type Alpha has been added.');
    $tds = $this->xpath('//td[@class = "menu-label" and text() = "Alpha"]');
    self::assertCount(1, $tds);
  }

  private static function assertRow(array $cells, string ...$expected): void {
    self::assertSame($expected[0], $cells[0]->getText());
    self::assertSame($expected[1], $cells[1]->getText());
    self::assertMatchesRegularExpression('# \d{2}/\d{2}/\d{4} #', $cells[2]->getText());
    self::assertSame($expected[2], $cells[3]->getText());
    self::assertSame($expected[3], $cells[4]->getText());
  }

  private static function createEvents(): void {
    SystemEventType::create(['id' => 'alpha', 'label' => 'Alpha'])->save();
    SystemEvent::create(['type' => 'alpha'])->save();
    // @phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
    SystemEvent::create(['type' => 'alpha', 'created' => \time() - 65, 'max_age' => 60])->save();
    SystemEvent::create(['type' => 'alpha'])->save();
  }

}
