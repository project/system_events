<?php

namespace Drupal\Tests\twig_tweak\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\system_events\Entity\SystemEvent;
use Drupal\system_events\Entity\SystemEventType;

/**
 * A test for System Event entity.
 *
 * @group system_events
 */
final class SystemEventTest extends KernelTestBase {

  protected static $modules = ['system_events'];

  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('system_event');
    SystemEventType::create(['id' => 'alpha', 'label' => 'Alpha'])->save();
  }

  public function testLabel(): void {
    $event = SystemEvent::create(['type' => 'alpha']);
    self::assertNull($event->label());
    $event->save();
    self::assertEquals('Event #1', $event->label());
  }

  public function testIsExpired(): void {
    $event = SystemEvent::create(['type' => 'alpha']);
    self::assertFalse($event->isExpired());
    $event->set('max_age', 0);
    \usleep(100_000);
    self::assertTrue($event->isExpired());
  }

}
