<?php

namespace Drupal\Tests\twig_tweak\Kernel;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\system_events\Entity\SystemEvent;
use Drupal\system_events\Entity\SystemEventType;

/**
 * A test for system_events_cron() hook.
 *
 * @group system_events
 */
final class CronTest extends KernelTestBase {

  protected static $modules = ['system_events'];

  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('system_event');
    SystemEventType::create(['id' => 'alpha', 'label' => 'Alpha'])->save();
  }

  public function testCron(): void {
    self::createEvent()->save();
    self::createEvent(['max_age' => 0])->save();
    self::createEvent(['max_age' => 10])->save();

    $events = SystemEvent::loadMultiple();
    self::assertSame([1, 2, 3], \array_keys($events));

    \usleep(100_000);
    $this->container->get('cron')->run();

    $events = SystemEvent::loadMultiple();
    self::assertSame([1, 3], \array_keys($events));

    $this->container->set('datetime.time', self::getTimeMock());
    // Call the function directly as the cron service does not work work with
    // the mocked 'datetime.time'.
    \system_events_cron();

    $events = SystemEvent::loadMultiple();
    self::assertSame([1], \array_keys($events));
  }

  private static function createEvent(array $values = []): SystemEvent {
    return SystemEvent::create(['type' => 'alpha'] + $values);
  }

  private static function getTimeMock(): TimeInterface {
    return new class extends Time {

      public function __construct() {}

      public function getCurrentTime(): int {
        return parent::getCurrentTime() + 1000;
      }

    };
  }

}
