# System Events

The module provides a fieldable entity type for logging system events.
System events intended to help developers to track execution of scheduled jobs.

## Installation
Install as usual.
```shell
composer require drupal/system_events
drush en system_events
```

## Configuration
Navigate to /admin/structure/system_event_types page and create event types
with needed fields. Optionally create bundle classes for them. That could be
done with `drush generate entity:bundle-class` command.

## Usage example
Suppose you want to send emails to subscribed users on weekly basis.
1. Create a new event type called 'newsletter'.
2. Add 'field_recipient' field that references a user account.
3. Add the following code to the hook_cron().

```php
// Part 1. Find user accounts that didn't get emails this week.
// Use DB service as EFQ does not support reverse relationships yet.
// @see https://www.drupal.org/node/2975750
$query = \Drupal::database()->select('users', 'u');
$query->leftJoin('system_event__field_recipient', 'fr', 'fr.field_recipient_target_id = u.uid AND fr.bundle = \'newsletter\'');
$query->fields('u', ['uid']);
$query->condition('u.uid', 0, '>');
$query->isNull('fr.entity_id');
$uids = $query->execute()->fetchCol();

// Part 2. Send the emails.
// It might be better to use Drupal queue API for this part.
foreach (User::loadMultiple($uids) as $user) {
  // Send email here.
  // ...

  // Create an event to remember that the user was sent the email.
  $event_values = [
    'type' => 'newsletter',
    'field_recipient' => $user->id(),
    'max_age' => 7 * 24 * 3600,
  ];
  SystemEvent::create($event_values)->save();
}
```
