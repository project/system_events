<?php declare(strict_types = 1);

namespace Drupal\system_events;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a system event entity type.
 */
interface SystemEventInterface extends ContentEntityInterface {

  /**
   * Determines if the event is expired.
   *
   * Expired events are removed automatically via CRON.
   */
  public function isExpired(): bool;

}
