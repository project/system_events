<?php declare(strict_types = 1);

namespace Drupal\system_events\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Time Interval' formatter.
 *
 * @FieldFormatter(
 *   id = "system_events_time_interval",
 *   label = @Translation("Time Interval"),
 *   field_types = {
 *     "integer"
 *   },
 * )
 */
final class TimeIntervalFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta]['#markup'] = self::getDateFormatter()->formatInterval($item->value);
    }
    return $element;
  }

  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getTargetEntityTypeId() === 'system_event' && $field_definition->getName() === 'max_age';
  }

  private static function getDateFormatter(): DateFormatterInterface {
    return \Drupal::service('date.formatter');
  }

}
