<?php declare(strict_types = 1);

namespace Drupal\system_events;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\system_events\Entity\SystemEvent;

/**
 * Item list definition for expired property.
 */
final class ExpiredItemList extends FieldItemList {

  use ComputedItemListTrait;

  protected function computeValue(): void {
    if (!\array_key_exists(0, $this->list)) {
      $event = $this->getEntity();
      \assert($event instanceof SystemEvent);
      $this->list[0] = $this->createItem(0, $event->isExpired());
    }
  }

}
