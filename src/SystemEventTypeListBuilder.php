<?php declare(strict_types = 1);

namespace Drupal\system_events;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of system event type entities.
 *
 * @see \Drupal\system_events\Entity\SystemEventType
 */
final class SystemEventTypeListBuilder extends ConfigEntityListBuilder {

  public function buildHeader(): array {
    $header['title'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity): array {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];
    return $row + parent::buildRow($entity);
  }

  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No system event types available. <a href=":link">Add system event type</a>.',
      [':link' => Url::fromRoute('entity.system_event_type.add_form')->toString()]
    );

    return $build;
  }

}
