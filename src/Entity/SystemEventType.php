<?php declare(strict_types = 1);

namespace Drupal\system_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the System Event type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "system_event_type",
 *   label = @Translation("System Event type"),
 *   label_collection = @Translation("System Event types"),
 *   label_singular = @Translation("system event type"),
 *   label_plural = @Translation("system events types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count system events type",
 *     plural = "@count system events types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\system_events\Form\SystemEventTypeForm",
 *       "edit" = "Drupal\system_events\Form\SystemEventTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\system_events\SystemEventTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer system event types",
 *   bundle_of = "system_event",
 *   config_prefix = "system_event_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/system_event_types/add",
 *     "edit-form" = "/admin/structure/system_event_types/manage/{system_event_type}",
 *     "delete-form" = "/admin/structure/system_event_types/manage/{system_event_type}/delete",
 *     "collection" = "/admin/structure/system_event_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
final class SystemEventType extends ConfigEntityBundleBase {

  /**
   * The machine name of this system event type.
   */
  protected string $id;

  /**
   * The human-readable name of the system event type.
   */
  protected string $label;

}
