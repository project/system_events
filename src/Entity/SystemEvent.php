<?php declare(strict_types = 1);

namespace Drupal\system_events\Entity;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\system_events\ExpiredItemList;
use Drupal\system_events\SystemEventInterface;

/**
 * Defines the system event entity class.
 *
 * @ContentEntityType(
 *   id = "system_event",
 *   label = @Translation("System Event"),
 *   label_collection = @Translation("System Events"),
 *   label_singular = @Translation("system event"),
 *   label_plural = @Translation("system events"),
 *   label_count = @PluralTranslation(
 *     singular = "@count system events",
 *     plural = "@count system events",
 *   ),
 *   bundle_label = @Translation("System Event type"),
 *   handlers = {
 *     "list_builder" = "Drupal\system_events\SystemEventListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\system_events\Routing\SystemEventRouteProvider",
 *     },
 *   },
 *   base_table = "system_event",
 *   admin_permission = "administer system event types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/reports/system-event",
 *     "canonical" = "/system-event/{system_event}",
 *   },
 *   bundle_entity_type = "system_event_type",
 *   field_ui_base_route = "entity.system_event_type.edit_form",
 * )
 */
class SystemEvent extends ContentEntityBase implements SystemEventInterface {

  use StringTranslationTrait;

  public function label(): string|TranslatableMarkup|NULL {
    return $this->isNew() ? parent::label() : $this->t('Event #@id', ['@id' => $this->id()]);
  }

  public function isExpired(): bool {
    return $this->get('max_age')->value !== NULL &&
      self::getTime()->getCurrentTime() - $this->get('created')->value > $this->get('max_age')->value;
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type']
      ->setLabel(\t('Type'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'settings' => ['link' => FALSE],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(\t('Created'))
      ->setDescription(t('The time that the system event was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['max_age'] = BaseFieldDefinition::create('integer')
      ->setLabel(\t('Max Age'))
      ->setDescription(\t('The maximum amount of time specified in the number of seconds the the event is allowed to be used.'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'system_events_time_interval',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['expired'] = BaseFieldDefinition::create('boolean')
      ->setLabel(\t('Expired'))
      ->setSetting('on_label', \t('Yes'))
      ->setSetting('off_label', \t('No'))
      ->setComputed(TRUE)
      ->setClass(ExpiredItemList::class)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 30,
      ]);

    return $fields;
  }

  private static function getTime(): TimeInterface {
    return \Drupal::service('datetime.time');
  }

}
