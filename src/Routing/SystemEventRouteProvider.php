<?php declare(strict_types = 1);

namespace Drupal\system_events\Routing;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides HTML routes for "System Event" entities.
 */
final class SystemEventRouteProvider implements EntityRouteProviderInterface, EntityHandlerInterface {

  private const CANONICAL_ROUTE = 'entity.system_event.canonical';

  public function __construct(private EntityRouteProviderInterface $innerProvider) {}

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new self(AdminHtmlRouteProvider::createInstance($container, $entity_type));
  }

  public function getRoutes(EntityTypeInterface $entity_type): RouteCollection|array {
    $routes = $this->innerProvider->getRoutes($entity_type);
    // Per the EntityRouteProviderInterface the $routes could be either a route
    // collection or an array of routes.
    (\is_array($routes) ? $routes[self::CANONICAL_ROUTE] : $routes->get(self::CANONICAL_ROUTE))
      ->setOption('_admin_route', TRUE);
    return $routes;
  }

}
