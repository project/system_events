<?php declare(strict_types = 1);

namespace Drupal\system_events;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the system event entity type.
 */
final class SystemEventListBuilder extends EntityListBuilder {

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    return new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );
  }

  protected function getEntityIds(): array {
    return $this->getStorage()->getQuery()
      ->accessCheck(FALSE)
      ->sort('created', 'desc')
      ->pager($this->limit)
      ->execute();
  }

  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['created'] = $this->t('Created');
    $header['max_age'] = $this->t('Max Age');
    $header['expired'] = $this->t('Expired');
    return $header;
  }

  public function buildRow(EntityInterface $entity): array {
    \assert($entity instanceof SystemEventInterface);
    $row['id'] = $entity->toLink($entity->id());
    $type_options = [
      'label' => 'hidden',
      'settings' => ['link' => FALSE],
    ];
    $row['type']['data'] = $entity->get('type')->view($type_options);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['max_age']['data'] = $entity->get('max_age')->view(['label' => 'hidden']);
    $row['expired']['data'] = $entity->get('expired')->view(['label' => 'hidden']);
    return $row;
  }

}
