<?php declare(strict_types = 1);

namespace Drupal\system_events\Command;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implements system-events:delete console command.
 */
final class DeleteCommand extends Command {

  protected function configure(): void {
    $this
      ->setName('system-events:delete')
      ->setDescription('Delete system event')
      ->addArgument('id', InputArgument::REQUIRED, 'Event ID');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {

    $event_id = $input->getArgument('id');
    $event = self::getEntityTypeManager()->getStorage('system_event')->load($event_id);
    if (!$event) {
      if ($output instanceof ConsoleOutputInterface) {
        $output = $output->getErrorOutput();
      }
      $message = \sprintf('[ERROR] Could not load system event with ID "%s".', $event_id);
      $output->writeln($message);
      return 1;
    }

    $event->delete();
    $output->writeln(\sprintf('Event "%s" has been removed.', $event_id));

    return 0;
  }

  private static function getEntityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::entityTypeManager();
  }

}
