<?php declare(strict_types = 1);

namespace Drupal\system_events\Command;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\Plugin\Field\FieldType\TimestampItem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implements system-events:view console command.
 */
final class ViewCommand extends Command {

  protected function configure(): void {
    $this
      ->setName('system-events:view')
      ->setDescription('View system event')
      ->addArgument('id', InputArgument::REQUIRED, 'Event ID');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {

    $event_id = $input->getArgument('id');
    $event = self::getEntityTypeManager()->getStorage('system_event')->load($event_id);
    if (!$event) {
      if ($output instanceof ConsoleOutputInterface) {
        $output = $output->getErrorOutput();
      }
      $message = \sprintf('[ERROR] Could not load system event with ID "%s".', $event_id);
      $output->writeln($message);
      return 1;
    }

    $rows = [];
    foreach ($event->getFieldDefinitions() as $name => $definition) {
      $rows[] = self::buildRow($definition, $event->get($name));
    }

    $header_style = new TableStyle();
    $header_style->setPadType(\STR_PAD_LEFT);
    (new Table($output))
      ->setStyle('symfony-style-guide')
      ->setColumnStyle(0, $header_style)
      ->setRows($rows)
      ->render();

    return 0;
  }

  private static function buildRow(FieldDefinitionInterface $definition, FieldItemListInterface $item_list): array {

    $name = \sprintf('<info>%s</info>:', $definition->getLabel());

    $formatted_items = [];
    foreach ($item_list as $item) {
      if ($item instanceof EntityReferenceItem) {
        $formatted_items[] = \sprintf('%s (%s)', $item->entity->label(), $item->entity->id());
        continue;
      }
      if ($item instanceof TimestampItem) {
        $formatted_items[] = self::getDateFormatter()->format($item->value, 'short');
        continue;
      }
      $formatted_items[] = $item->getString();
    }
    $value = \implode(\PHP_EOL, $formatted_items);

    return [$name, $value];
  }

  private static function getEntityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::entityTypeManager();
  }

  private static function getDateFormatter(): DateFormatterInterface {
    return \Drupal::service('date.formatter');
  }

}
