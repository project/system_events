<?php declare(strict_types = 1);

namespace Drupal\system_events\Command;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\system_events\SystemEventInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implements system-events:list console command.
 */
final class ListCommand extends Command {

  protected function configure(): void {
    $this
      ->setName('system-events:list')
      ->setDescription('List system events')
      ->addOption('type', mode: InputOption::VALUE_OPTIONAL, description: 'Machine name of the event type')
      ->addOption('limit', mode: InputOption::VALUE_OPTIONAL, description: 'Maximum number of records to print.');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    $events = self::getEvents($input->getOption('type'), $input->getOption('limit'));

    $rows = \array_map([self::class, 'buildRow'], $events);
    (new Table($output))
      ->setHeaders(['ID', 'Type', 'Created'])
      ->setRows($rows)
      ->render();

    return 0;
  }

  private static function getEvents(?string $type, ?string $limit): array {
    $storage = self::getEntityTypeManager()->getStorage('system_event');
    $query = $storage->getQuery();
    $query->accessCheck(FALSE);
    if ($type) {
      $query->condition('type', $type);
    }
    if ($limit) {
      $query->range(0, $limit);
    }
    $query->sort('created', 'DESC');

    $ids = $query->execute();
    return $storage->loadMultiple($ids);
  }

  private static function buildRow(SystemEventInterface $event): array {
    return [
      $event->id(),
      $event->get('type')->entity->label(),
      self::getDateFormatter()->format($event->get('created')->value, 'short'),
    ];
  }

  private static function getEntityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::entityTypeManager();
  }

  private static function getDateFormatter(): DateFormatterInterface {
    return \Drupal::service('date.formatter');
  }

}
