<?php declare(strict_types = 1);

namespace Drupal\system_events\Command;

use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implements system-events:purge console command.
 */
final class PurgeCommand extends Command {

  protected function configure(): void {
    $this
      ->setName('system-events:purge')
      ->setDescription('Purge all system events');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    $storage = self::getStorage();
    $events = $storage->loadMultiple();
    $storage->delete($storage->loadMultiple());

    $output->writeln(\sprintf('Removed events: %d', \count($events)));
    return 0;
  }

  private static function getStorage(): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage('system_event');
  }

}
