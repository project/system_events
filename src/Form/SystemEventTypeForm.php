<?php

namespace Drupal\system_events\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for system event type forms.
 */
final class SystemEventTypeForm extends BundleEntityFormBase {

  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $entity_type = $this->entity;
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label system event type', ['%label' => $entity_type->label()]);
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this system event type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\system_events\Entity\SystemEventType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this system event type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  public function save(array $form, FormStateInterface $form_state): int {
    $entity_type = $this->entity;

    $entity_type->set('id', \trim($entity_type->id()));
    $entity_type->set('label', \trim($entity_type->label()));

    $result = $entity_type->save();

    $t_args = ['%name' => $entity_type->label()];
    $message = match ($result) {
      \SAVED_NEW => $this->t('The system event type %name has been added.', $t_args),
      \SAVED_UPDATED => $this->t('The system event type %name has been updated.', $t_args),
    };
    $this->messenger()->addStatus($message);

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
    return $result;
  }

}
